##@Krishna Somandepalli - Face detection - alignment - etc.
import os,sys,json, cv2
from pylab import *

from faceswap import *

#im_dir = sys.argv[1]
# perform both intra- and inter-subject normalization - 
# pick the first frame of the first person as the reference template!!
# you can do this more systematically if you wish to
DLIB_ARRAY = []
SAVE_FACE = True

REF_IMG_PATH = '/home/krsna/workspace/2016_avec/data/recordings_video/train_1_PNG/0001.png'
REF_IMG, REF_LMARKS, REF_BBOX = read_im_and_landmarks(REF_IMG_PATH)
x1,y1,x2,y2 = REF_BBOX
out_name = os.path.join(os.path.dirname(REF_IMG_PATH), 'face_0001.png')
if SAVE_FACE: cv2.imwrite(out_name, REF_IMG[y1:y2, x1:x2,:])
face_dict = {}
face_dict["fpath"] = REF_IMG_PATH
face_dict["lmarks"] = REF_LMARKS.tolist()
face_dict["bbox"] = REF_BBOX
face_dict["ref_bbox"] = REF_BBOX
DLIB_ARRAY.append(face_dict)

base_dir = '/home/krsna/workspace/2016_avec/data/recordings_video/'

#base_dir_ls = [i for i in os.listdir(base_dir) if i.endswith('PNG')]

#if reading a video directly - do it here
base_dir_ls = [sys.argv[1]]
for png_dir in base_dir_ls:
	print 'now - processing - ',
	png_dirpath = os.path.join(base_dir,png_dir)
	print png_dirpath
	png_ls = sorted([i for i in os.listdir(png_dirpath) \
			if not i.startswith('face') and i.endswith('.png')])
	for png_i, png_fname in enumerate(png_ls):
		l2 = None
		png_fpath = os.path.join(png_dirpath,png_fname)
		if not png_i%1000: print png_fpath
		try: im2,l2,b2 = read_im_and_landmarks(png_fpath)
		except: NoFaces
		if l2 is not None:
			face_dict = {}
			# if using a video - save the frame number instead
			face_dict["fpath"] = png_fpath
			M = transformation_from_points(REF_LMARKS[ALIGN_POINTS],l2[ALIGN_POINTS])
			warped_im2 = warp_im(im2, M, REF_IMG.shape)
			l_warped = None; b_warped = None
			try: l_warped, b_warped = get_landmarks(warped_im2)
			except: NoFaces
			if l_warped is not None: face_dict["lmarks"] = l_warped.tolist()
			else: face_dict["lmarks"] = None
			face_dict["bbox"] = b_warped
			face_dict["ref_bbox"] = REF_BBOX
			out_name = os.path.join(png_dirpath,'face_'+png_fname)
			if SAVE_FACE: cv2.imwrite(out_name, warped_im2[y1:y2, x1:x2,:])
			DLIB_ARRAY.append(face_dict)

with open('%s_dlib_face_details_06_03.json' % (base_dir_ls[0]),'w') as F:
	json.dump(DLIB_ARRAY, F)	
			
